package com.im.server.general.common.menu;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.im.server.general.common.bean.system.Menu;


/**
 * date 2018-07-12 23:03:26<br>
 * description
 * 
 * @author XiaHui<br>
 * @since
 */
public class MenuBox {

	public static List<Menu> getRootMenuList() {
		List<Menu> list = new ArrayList<Menu>();

		createMenu(list, "index", "首页导航");
		createMenu(list, "personal", "个人信息");
		createMenu(list, "system", "系统权限");
		createMenu(list, "core", "用户数据");
		return list;
	}

	static void createMenu(List<Menu> list, String id, String name) {
		createMenu(list, id, name, "0");
	}

	static void createMenu(List<Menu> list, String id, String name, String superId) {

		Menu menu = new Menu();
		menu.setId(id);
		menu.setName(name);
		menu.setFlag("1");
		menu.setSuperId(superId);
		menu.setType(Menu.type_menu);
		menu.setCreateTime(new Date());
		list.add(menu);
		menu.setRank(list.indexOf(menu));
	}
}
