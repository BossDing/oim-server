/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.im.server.general.common.bean.system;

import java.util.Date;

import javax.persistence.Entity;

import com.im.base.bean.BaseBean;

/**
 * 
 * @author xiahui
 */
@Entity(name = "m_user_role")
public class UserRole extends BaseBean {

	private String userId;
	private String roleId;// 角色Id
	private Date createTime;// 输入时间
	private int rank;// 排序


	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getRoleId() {
		return roleId;
	}

	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public int getRank() {
		return rank;
	}

	public void setRank(int rank) {
		this.rank = rank;
	}

}
