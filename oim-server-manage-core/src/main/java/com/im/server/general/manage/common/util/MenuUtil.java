package com.im.server.general.manage.common.util;

/**
 * date 2018-07-17 22:10:46<br>
 * description
 * 
 * @author XiaHui<br>
 * @since
 */
public class MenuUtil {

	public static String getKey(String key, String space) {
		String id = "";
		if (null != key) {
			if (key.startsWith("/")) {
				key = key.substring(1, key.length());
			}
			if (null != space) {
				id = key.replace("/", space);
			}
		}
		return id;
	}
}
