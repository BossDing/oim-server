import service from '@/libs/service';

let group = {};

group.list = function (query, page, back) {
    var body = {
        'groupQuery': query,
        'page': page
    };
    service.postBody('/manage/core/group/list', body, back);
};

export default group;
