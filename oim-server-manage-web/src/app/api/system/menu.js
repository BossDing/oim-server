import service from '@/libs/service';

let menu = {};

menu.allList = function (back) {
    var body = {};
    var m = {'body': body};
    service.post('/manage/system/menu/allList', m, back);
}

menu.refresh = function (back) {
    var body = {};
    var m = {'body': body};
    service.post('/manage/system/menu/refresh', m, back);
}

menu.updateFlag = function (id, flag, back) {
    var body = {
        'id': id,
        'flag': flag
    };
    var m = {'body': body};
    service.post('/manage/system/menu/updateFlag', m, back);
}

export default menu;
