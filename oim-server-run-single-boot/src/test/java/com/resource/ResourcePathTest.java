package com.resource;

import java.io.InputStream;

import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

public class ResourcePathTest {

	public static void main(String[] args) {
		try {
			String resourcePath="classpath*:dao/*/query/mysql/*.xml";
			PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
			Resource[] resources = resolver.getResources(resourcePath);
			if (resources != null && resources.length > 0) {
				for (Resource resource : resources) {
					InputStream input = resource.getInputStream();
					try {
						System.out.println(resource.getFilename());
					} catch (Exception e) {
						e.printStackTrace();
					} finally {
						input.close();
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
